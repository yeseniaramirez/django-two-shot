from django.urls import path

from receipts.views import (
    ReceiptCreateView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptListView,
    AccountListView,
    ExpenseCategoryListView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/", ExpenseCategoryListView.as_view(), name="category_list"
    ),
]
